package com.bxacosta;

import com.bxacosta.servicios.HolaMundo;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Stream;

@WebServlet(urlPatterns = "/test")
public class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext sc = req.getServletContext();

        Object obj =sc.getAttribute(WebApplicationContext.CONTEXT_ATTRIBUTES_BEAN_NAME);
        ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(sc);
        HolaMundo servicio = context.getBean("holaMundoImpl", HolaMundo.class);


        PrintWriter pw = resp.getWriter();
        Stream.of(context.getBeanDefinitionNames()).forEach(pw::println);
        pw.println(servicio.hola("Mundo"));
    }
}
