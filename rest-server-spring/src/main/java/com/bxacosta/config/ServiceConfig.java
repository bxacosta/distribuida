package com.bxacosta.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.bxacosta.service"})
public class ServiceConfig {
}
