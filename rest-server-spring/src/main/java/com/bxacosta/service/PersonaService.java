package com.bxacosta.service;

import com.bxacosta.model.Persona;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonaService {

    private List<Persona> personas;

    public PersonaService() {
        this.personas = new ArrayList<>();

        personas.add(Persona.builder().id(1).nombre("Bryan").build());
        personas.add(Persona.builder().id(2).nombre("Xavier").build());
        personas.add(Persona.builder().id(3).nombre("Andres").build());
    }

    public Persona findById(Integer id) {
        return personas.stream().filter(p -> p.getId() == id).findFirst().orElse(null);
    }

    public List<Persona> findAll() {
        return personas;
    }

    public Persona save(Persona item) {
        Persona p = this.findById(item.getId());
        if (p != null) {
            int index = personas.indexOf(p);
            personas.set(index, item);
        } else {
            item.setId(personas.size());
            personas.add(item);
        }
        return item;
    }

    public void delete(Integer id) {
        this.personas = personas.stream().filter(p -> !(p.getId() == id)).collect(Collectors.toList());
    }
}
