package com.bxacosta.config;

import com.bxacosta.servicios.HolaMundo;
import com.bxacosta.servicios.HolaMundoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.bxacosta.servicios")
public class ConfiguracionContenedor {
    @Bean
    public HolaMundo hola() {
        return  new HolaMundoImpl();
    }
}
