package com.bxacosta;

import com.bxacosta.config.ConfiguracionContenedor;
import com.bxacosta.servicios.HolaMundo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

public class Principal {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ConfiguracionContenedor.class);

        HolaMundo servicio1 = context.getBean("hola", HolaMundo.class);
        HolaMundo servicio2 = context.getBean("holaMundoImpl", HolaMundo.class);
        System.out.println(servicio1.hola("Mundo1"));
        System.out.println(servicio2.hola("Mundo2"));

        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
    }
}
