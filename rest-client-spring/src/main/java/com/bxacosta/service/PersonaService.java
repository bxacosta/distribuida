package com.bxacosta.service;

import com.bxacosta.model.Persona;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Service
public class PersonaService {

    private static final String BASE_URL = "http://127.0.0.0:8080/api/personas";
    private final RestTemplate template;

    public PersonaService(RestTemplate template) {
        this.template = template;
    }

    public Persona findById(Integer id) {
        return template.getForObject(BASE_URL + "/{id}", Persona.class, id);
    }

    public List<Persona> findAll() {
        ResponseEntity<Persona[]> response = template.getForEntity(BASE_URL, Persona[].class);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }

    public Persona save(Persona item) {
        ResponseEntity<Persona> response = template.postForEntity(BASE_URL, item, Persona.class);
        return response.getBody();
    }

    public void delete(Integer id) {
        template.delete(BASE_URL + "/{id}", id);
    }
}
