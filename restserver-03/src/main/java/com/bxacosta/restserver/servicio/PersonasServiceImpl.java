package com.bxacosta.restserver.servicio;

import com.bxacosta.restserver.dto.Persona;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/personas")
public class PersonasServiceImpl {

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Persona buscar(@PathParam(value = "id") int id) {

        Persona p = new Persona();

        p.setId(id);
        p.setNombre(String.format("persona-%d", id));

        return p;
    }
}
