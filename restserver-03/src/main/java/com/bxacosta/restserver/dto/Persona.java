package com.bxacosta.restserver.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name = "persona")
public class Persona implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int id;
    @Getter
    @Setter
    private String nombre;
}
