<%--
  Created by IntelliJ IDEA.
  User: xavier
  Date: 27/05/19
  Time: 17:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Operaciones</title>
</head>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-12">
            <form method="POST" action="operacion">
                <h2 class="mb-3">Operaciones</h2>
                <div class="form-group">
                    <label>Número 1</label>
                    <input name="n1" type="number" class="form-control">
                </div>
                <div class="form-group">
                    <label>Número 2</label>
                    <input name="n2" type="number" class="form-control">
                </div>
                <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </div>
                <label>Respuesta: ${resultado}</label>
            </form>
        </div>
    </div>
</div>
</body>
</html>
