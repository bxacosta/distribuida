package com.bxacosta.servlet;

import com.bxacosta.service.ServicioOperaciones;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/operacion")
public class IndexServlet extends HttpServlet {

    private ServicioOperaciones operaciones;

    @Override
    public void init() throws ServletException {
        AnnotationConfigApplicationContext context = (AnnotationConfigApplicationContext) getServletContext().getAttribute("context");
        operaciones = context.getBean(ServicioOperaciones.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/operaciones.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int n1 = Integer.parseInt(req.getParameter("n1"));
        int n2 = Integer.parseInt(req.getParameter("n2"));
        int suma  = operaciones.sumar(n1, n2);
        req.setAttribute("resultado", suma);
        doGet(req, resp);
    }
}
