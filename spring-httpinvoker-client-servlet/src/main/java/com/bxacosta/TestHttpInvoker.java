package com.bxacosta;

import com.bxacosta.config.HttpInvokerConfig;
import com.bxacosta.service.ServicioOperaciones;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestHttpInvoker {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HttpInvokerConfig.class);

        ServicioOperaciones operaciones = context.getBean(ServicioOperaciones.class);

        System.out.println("Servicio: " + operaciones.getClass());

        int respuesta = operaciones.sumar(4, 3);

        System.out.println("Respuesta: " + respuesta);
    }
}
