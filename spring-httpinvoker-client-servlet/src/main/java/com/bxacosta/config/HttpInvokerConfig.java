package com.bxacosta.config;


import com.bxacosta.service.ServicioOperaciones;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@Configuration
public class HttpInvokerConfig {

    private static final String SERVICE_NAME = "http://localhost:8081/operaciones";

    @Bean
    public HttpInvokerProxyFactoryBean exporter() {
        HttpInvokerProxyFactoryBean proxy = new HttpInvokerProxyFactoryBean();

        proxy.setServiceUrl(SERVICE_NAME);
        proxy.setServiceInterface(ServicioOperaciones.class);

        return proxy;
    }
}
