package com.bxacosta.cloudclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Value("${message}")
    private String mensaje;

    @Value("${server.port:8080}")
    private String port;

    @GetMapping(path="/hola/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String hola(@PathVariable("nombre") String nombre) {
        return String.format("%s: %s: %s", mensaje, nombre, port);
    }
}
