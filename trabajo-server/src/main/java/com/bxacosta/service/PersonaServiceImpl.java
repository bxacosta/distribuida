package com.bxacosta.service;

import com.bxacosta.dto.Persona;
import com.bxacosta.mapper.PersonaMapper;
import com.bxacosta.util.SpringUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@Remote(PersonaService.class)
public class PersonaServiceImpl implements PersonaService {

    private JdbcTemplate jdbcTemplate;

    public PersonaServiceImpl() {
        jdbcTemplate = SpringUtils.getJdbcTemplate();
    }

    @Override
    public Persona save(Persona item) {
        if (item.getId() != null) {
            Persona persona = findById(item.getId());
            if (persona != null) {
                String query = "UPDATE persona SET identificacion = ?, nombre = ?, fechaNacimiento = ?, direccion = ? WHERE id = ?";
                jdbcTemplate.update(query, item.getIdentificacion(), item.getNombre(), item.getFechaNacimiento(), item.getDireccion(), item.getId());
                return item;
            }
        }

        String query = "INSERT INTO persona (identificacion, nombre, fechaNacimiento, direccion) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(query, item.getIdentificacion(), item.getNombre(), item.getFechaNacimiento(), item.getDireccion());
        return item;
    }

    @Override
    public Persona findById(Integer id) {
        String query = "SELECT * FROM persona WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new PersonaMapper());
    }

    @Override
    public List<Persona> findAll() {
        String query = "SELECT * FROM persona";
        return jdbcTemplate.query(query, new PersonaMapper());
    }

    @Override
    public void delete(Integer id) {
        String query = "DELETE FROM persona WHERE id = ?";
        jdbcTemplate.update(query, id);
    }
}
