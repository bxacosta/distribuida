package com.bxacosta.service;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

@CommonsLog
@Component
public class MessageListener {

    private final PersonaService personaService;

    public MessageListener(PersonaService personaService) {
        this.personaService = personaService;
    }

    @JmsListener(destination = "persona.delete", containerFactory = "jmsListenerContainerFactory")
    public void onMessage(Message message) throws JMSException {

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            Integer id = Integer.parseInt(textMessage.getText());

            log.info("Eliminaodo persona con id: " + id);
            personaService.delete(id);
        } else {
            System.out.println("Message is not a instance of TextMessage or ObjectMessage");
        }
    }
}
