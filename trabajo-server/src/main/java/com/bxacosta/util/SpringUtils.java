package com.bxacosta.util;

import com.bxacosta.config.ServerConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class SpringUtils {

    private static AnnotationConfigApplicationContext CONTEXT_INSTANCE;

    public static void initContext() {
        CONTEXT_INSTANCE = new AnnotationConfigApplicationContext(ServerConfig.class);
    }

    public static void closeContext() {
        if (CONTEXT_INSTANCE != null) {
            CONTEXT_INSTANCE.close();
        }
    }

    public static JdbcTemplate getJdbcTemplate() {
        if (CONTEXT_INSTANCE == null) {
            initContext();
        }
        return CONTEXT_INSTANCE.getBean(JdbcTemplate.class);
    }
}
