package com.bxacosta.listener;

import com.bxacosta.util.SpringUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        SpringUtils.initContext();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        SpringUtils.closeContext();
    }
}