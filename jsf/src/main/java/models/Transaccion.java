package models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Transaccion {

    private String origen;
    private String destino;
    private Double monto;

    public Transaccion() {
        this.monto = 0.0;
    }
}
