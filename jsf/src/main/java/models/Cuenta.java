package models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cuenta {

    private String nombre;
    private String cuenta;
    private Double saldo;

    public Cuenta() {
        this.saldo = 0.0;
    }
}
