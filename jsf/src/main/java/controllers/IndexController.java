package controllers;

import lombok.Getter;
import lombok.Setter;
import models.Cuenta;
import models.Transaccion;
import services.CuentaService;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.List;

@ManagedBean
@RequestScoped
public class IndexController implements Serializable {

    @Setter
    @ManagedProperty(value = "#{cuentaService}")
    private CuentaService cuentaService;

    @Getter
    @Setter
    private Cuenta cuenta;
    @Getter
    @Setter
    private Transaccion transaccion;
    @Getter
    @Setter
    private List<Cuenta> cuentas;

    @PostConstruct
    public void init() {
        this.cuenta = new Cuenta();
        this.transaccion = new Transaccion();
        this.cuentas = cuentaService.list();
    }

    public void addCuenta() {
        cuentaService.save(cuenta);
        cuenta = new Cuenta();
    }

    public void transferir() {
        try {
            cuentaService.retirar(transaccion.getOrigen(), transaccion.getMonto());
            cuentaService.depositar(transaccion.getDestino(), transaccion.getMonto());
        } catch (ValidatorException e) {
            FacesContext.getCurrentInstance().addMessage(null, e.getFacesMessage());
        }
    }
}
