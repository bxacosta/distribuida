package services;

import models.Cuenta;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.validator.ValidatorException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@SessionScoped
public class CuentaService implements Serializable {

    private List<Cuenta> cuentas;

    @PostConstruct
    public void init() {
        cuentas = new ArrayList<>();
    }

    public void save(Cuenta cuenta) {
        this.cuentas.add(cuenta);
    }

    private Cuenta findByCuenta(String cuenta) {
        return this.cuentas.stream().filter(c -> c.getCuenta().equals(cuenta)).findFirst().orElse(null);
    }

    public List<Cuenta> list() {
        return cuentas;
    }

    public void retirar(String cuenta, Double monto) {
        Cuenta c = this.findByCuenta(cuenta);

        if (c != null) {
            if (c.getSaldo() >= monto) {
                c.setSaldo(c.getSaldo() - monto);
            } else {
                throw new ValidatorException(new FacesMessage("Saldo insuficiente"));
            }
        } else {
            throw new ValidatorException(new FacesMessage("Cuenta \"" + cuenta + "\" no existe"));
        }
    }

    public void depositar(String cuenta, Double monto) {
        Cuenta c = this.findByCuenta(cuenta);

        if (c != null) {
            c.setSaldo(c.getSaldo() + monto);
        } else {
            throw new ValidatorException(new FacesMessage("Cuenta \"" + cuenta + "\" no existe"));
        }
    }
}
