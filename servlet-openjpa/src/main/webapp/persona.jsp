<%@ page import="com.bxacosta.entities.Persona" %>
<%@ page import="java.util.List" %>
<%@ page import="com.bxacosta.models.Message" %><%--
  Created by IntelliJ IDEA.
  User: xavier
  Date: 13/05/19
  Time: 16:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <title>Persona</title>
</head>
<body>
<div class="container mt-3">
    <div class="row">
        <div class="col-12">
            <%
                if (request.getAttribute("message") != null) {
            %>
            <div class="alert ${(message.type eq "error")? "alert-danger" : "alert-success"}" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                ${message.text}
            </div>
            <%}%>
        </div>

        <div class="col-6">
            <form method="POST" action="persona">
                <input name="id" type="hidden" value="${persona.id}">
                <h2 class="mb-2">Datos Persona</h2>
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="nombre" type="text" class="form-control" value="${persona.nombre}">
                </div>
                <div class="form-group">
                    <label>Dirección</label>
                    <input name="direccion" type="text" class="form-control" value="${persona.direccion}">
                </div>
                <div class="form-group ">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </form>
        </div>
        <div class="col-6">
            <h2 class="mb-2">Personas</h2>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Direccion</th>
                    <th>Accion</th>
                </tr>
                </thead>
                <tbody>

                <%
                    List<Persona> personas = (List<Persona>) request.getAttribute("personas");
                    for (Persona p : personas) {
                %>
                <tr>
                    <td><%= p.getId()%>
                    </td>
                    <td><%= p.getNombre()%>
                    </td>
                    <td><%= p.getDireccion()%>
                    </td>
                    <td>
                        <a href="persona?id=<%= p.getId()%>" class="p-0">Editar</a>
                        <form method="post" action="persona" class="m-0">
                            <input name="delete" type="hidden" value="<%= p.getId()%>">
                            <button type="submit" class="btn btn-link text-danger p-0">Eliminar</button>
                        </form>
                    </td>
                </tr>
                <%}%>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
