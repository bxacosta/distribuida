package com.bxacosta.services;

import com.bxacosta.entities.Persona;

import javax.persistence.EntityManager;
import java.util.List;

public class PersonaServiceImpl implements PersonaService{
    private EntityManager em;

    public PersonaServiceImpl(EntityManager em) {
        this.em = em;
    }

    @Override
    public Persona save(Persona item) {
        try {
            Persona newPersona;
            if (item.getId() == null) { //new item
                em.getTransaction().begin();
                em.persist(item);
                em.getTransaction().commit();
            } else { // update item
                Persona saved = em.find(Persona.class, item.getId());
                if (saved != null) {
                    em.getTransaction().begin();
                    em.merge(item);
                    em.getTransaction().commit();
                }
            }
            return item;
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
        }
    }

    @Override
    public Persona findById(Integer id) {
        try {
            em.getTransaction().begin();
            Persona item = em.find(Persona.class, id);
            em.getTransaction().commit();
            return item;
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
        }
    }

    @Override
    public List<Persona> findAll() {
        try {
            em.getTransaction().begin();
            List<Persona> list = em.createQuery("SELECT p FROM Persona p", Persona.class).getResultList();
            em.getTransaction().commit();
            return list;
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Persona item = em.find(Persona.class, id);
            if (item != null) {
                em.getTransaction().begin();
                em.remove(item);
                em.getTransaction().commit();
            }
        } finally {
            if (em.getTransaction().isActive())
                em.getTransaction().rollback();
        }
    }
}
