package com.bxacosta.services;

import com.bxacosta.entities.Persona;

import java.util.List;

public interface PersonaService {
    Persona save(Persona item);
    Persona findById(Integer id);
    List<Persona> findAll();
    void delete(Integer id);
}
