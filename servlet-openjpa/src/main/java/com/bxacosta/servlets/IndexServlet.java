package com.bxacosta.servlets;

import com.bxacosta.entities.Persona;
import com.bxacosta.models.Message;
import com.bxacosta.services.PersonaServiceImpl;
import org.apache.openjpa.lib.util.StringUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/persona")
public class IndexServlet extends HttpServlet {

    private PersonaServiceImpl personaService;

    @Override
    public void init() throws ServletException {
        EntityManagerFactory emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
        EntityManager em = emf.createEntityManager();

        personaService = new PersonaServiceImpl(em);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Persona persona = new Persona();
        if (!StringUtil.isEmpty(req.getParameter("id"))) {
            persona = personaService.findById(Integer.parseInt(req.getParameter("id")));
        }
        req.setAttribute("persona", persona);
        req.setAttribute("personas", personaService.findAll());
        req.getRequestDispatcher("/persona.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (!StringUtil.isEmpty(req.getParameter("delete"))) {
            personaService.delete(Integer.parseInt(req.getParameter("delete")));
            req.setAttribute("message", new Message("info", "Persona eliminada correctamente"));
            doGet(req, resp);
            return;
        }

        Persona persona = new Persona();
        if (!StringUtil.isEmpty(req.getParameter("id"))) {
            persona.setId(Integer.parseInt(req.getParameter("id")));
        }
        persona.setNombre(req.getParameter("nombre"));
        persona.setDireccion(req.getParameter("direccion"));

        personaService.save(persona);
        req.setAttribute("message", new Message("info", "Persona guardada correctamente"));
        doGet(req, resp);
    }
}
