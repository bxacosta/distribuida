package com.bxacosta.cloudconsumer.controller;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    private final DiscoveryClient discoveryClient;

    public MainController(DiscoveryClient discoveryClient) {
        this.discoveryClient = discoveryClient;
    }

    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public String test() {
        var instances = discoveryClient.getInstances("client");

        instances.forEach(i -> {
            System.out.println(i.getHost());
            System.out.println(i.getPort());
            System.out.println(i.getUri());
        });
        return "Instancias de 'client': " +  instances.size();
    }

}
