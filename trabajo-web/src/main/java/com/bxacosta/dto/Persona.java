package com.bxacosta.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Persona implements Serializable {
    private Integer id;
    private String identificacion;
    private String nombre;
    private Date fechaNacimiento;
    private String direccion;
}
