package com.bxacosta.listener;

import com.bxacosta.config.ClientConfig;
import com.bxacosta.service.PersonaService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Hashtable;

@WebListener
@CommonsLog
public class AppListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfig.class);
        sce.getServletContext().setAttribute("context", context);

        final Hashtable<String, String> jndiProperties = new Hashtable<>();
        jndiProperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
        jndiProperties.put(Context.PROVIDER_URL, "remote+http://127.0.0.1:8080");

        try {
            final Context ic = new InitialContext(jndiProperties);
            PersonaService personaService = (PersonaService) ic.lookup("ejb:/examen-server-1.0-SNAPSHOT/PersonaServiceImpl!"
                    + PersonaService.class.getName());

            sce.getServletContext().setAttribute("personaService", personaService);
        } catch (NamingException e) {
            log.error("error al obtener EJB remoto", e);
        }

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        AnnotationConfigApplicationContext context = (AnnotationConfigApplicationContext) sce.getServletContext().getAttribute("context");
        context.close();
    }
}