package com.bxacosta.listener;

import com.bxacosta.model.Numeros;
import com.bxacosta.service.MessageSender;
import com.bxacosta.service.ServicioOperaciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

@Component("messageListener")
public class MessageListener {

    private final MessageSender messageSender;
    private final ServicioOperaciones operaciones;

    public MessageListener(MessageSender messageSender, ServicioOperaciones operaciones) {
        this.messageSender = messageSender;
        this.operaciones = operaciones;
    }

    @JmsListener(destination = "producer", containerFactory = "jmsListenerContainerFactory")
    public void onMessage(Message message) throws JMSException {

        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;

            System.out.println(">>> Text Received : " + textMessage.getText());
        } else if (message instanceof ObjectMessage){
            ObjectMessage objectMessage = (ObjectMessage) message;

            System.out.println(">>> Object Received: " + objectMessage.getObject().toString());

            Numeros numeros = (Numeros) objectMessage.getObject();

            Integer result = operaciones.sumar(numeros.getN1(), numeros.getN2());

            messageSender.sendMessage(result.toString());
        } else {
            System.out.println("Message is not a instance of TextMessage or ObjectMessage");
        }
    }
}
