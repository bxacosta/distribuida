package com.bxacosta.service;

public interface MessageSender {

    void sendMessage(String message);

}
