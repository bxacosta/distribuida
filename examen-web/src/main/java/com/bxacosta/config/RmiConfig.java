package com.bxacosta.config;

import com.bxacosta.service.PersonaService;
import org.springframework.context.annotation.Bean;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

public class RmiConfig {

    private static final String SERVICE_NAME = "rmi://localhost:1099/persona-rmi";

    @Bean
    public RmiProxyFactoryBean personaRmi() {
        RmiProxyFactoryBean proxy = new RmiProxyFactoryBean();

        proxy.setServiceUrl(SERVICE_NAME);
        proxy.setServiceInterface(PersonaService.class);

        return proxy;
    }

}
