package com.bxacosta.service;


import lombok.extern.apachecommons.CommonsLog;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

@CommonsLog
@Service
public class MessageSenderImpl implements MessageSender {

    private final JmsTemplate jmsTemplate;

    public MessageSenderImpl(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public void sendMessage(String message) {
        jmsTemplate.setDeliveryDelay(5000L);

        this.jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage jmsMessage = session.createTextMessage(message);
                log.info("Enviando mensaje: " + jmsMessage.getText());
                return jmsMessage;
            }
        });
    }

}
