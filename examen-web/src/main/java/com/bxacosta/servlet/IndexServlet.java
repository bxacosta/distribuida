package com.bxacosta.servlet;

import com.bxacosta.dto.Persona;
import com.bxacosta.model.Message;
import com.bxacosta.service.MessageSender;
import com.bxacosta.service.PersonaService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/persona")
public class IndexServlet extends HttpServlet {

    private PersonaService personaService;
    private MessageSender messageSender;

    @Override
    public void init() throws ServletException {
        AnnotationConfigApplicationContext context = (AnnotationConfigApplicationContext) getServletContext().getAttribute("context");
        personaService = context.getBean(PersonaService.class);
        messageSender = context.getBean(MessageSender.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Persona persona = new Persona();
        if (req.getParameter("id") != null && !req.getParameter("id").equals("")) {
            persona = personaService.findById(Integer.parseInt(req.getParameter("id")));
        }
        req.setAttribute("persona", persona);
        req.setAttribute("personas", personaService.findAll());
        req.getRequestDispatcher("/persona.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("delete") != null && !req.getParameter("delete").equals("")) {
            messageSender.sendMessage(req.getParameter("delete"));
            req.setAttribute("message", new Message("info", "Su requerimiento a sido enviado"));
            doGet(req, resp);
            return;
        }

        Persona persona = new Persona();
        if (req.getParameter("id") != null && !req.getParameter("id").equals("")) {
            persona.setId(Integer.parseInt(req.getParameter("id")));
        }
        persona.setDireccion(req.getParameter("identificacion"));
        persona.setNombre(req.getParameter("nombre"));
        persona.setDireccion(req.getParameter("direccion"));

        personaService.save(persona);
        req.setAttribute("message", new Message("info", "Persona guardada correctamente"));
        doGet(req, resp);
    }
}
