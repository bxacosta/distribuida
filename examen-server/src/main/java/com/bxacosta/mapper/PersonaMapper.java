package com.bxacosta.mapper;

import com.bxacosta.dto.Persona;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonaMapper implements RowMapper<Persona> {

    @Override
    public Persona mapRow(ResultSet rs, int rowNum) throws SQLException {
        Persona persona = new Persona();

        persona.setId(rs.getInt("id"));
        persona.setIdentificacion(rs.getString("identificacion"));
        persona.setNombre(rs.getString("nombre"));
        persona.setFechaNacimiento(rs.getDate("fechaNacimiento"));
        persona.setDireccion(rs.getString("direccion"));

        return persona;
    }
}
