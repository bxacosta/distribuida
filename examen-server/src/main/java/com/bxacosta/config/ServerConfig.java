package com.bxacosta.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ArtemisConfig.class, DataServiceConfig.class, RmiConfig.class})
@ComponentScan(basePackages = "com.bxacosta.service")
public class ServerConfig {
}
