package com.bxacosta.config;

import com.bxacosta.service.PersonaService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

@Configuration
public class RmiConfig {

    @Bean
    public RmiServiceExporter personaRmi(PersonaService personaService) {
        RmiServiceExporter exporter = new RmiServiceExporter();

        exporter.setServiceName("persona-rmi");
        exporter.setService(personaService);
        exporter.setServiceInterface(PersonaService.class);

        return exporter;
    }
}
