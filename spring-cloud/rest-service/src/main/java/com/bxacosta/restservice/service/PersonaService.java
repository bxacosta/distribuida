package com.bxacosta.restservice.service;

import com.bxacosta.restservice.entity.Persona;
import com.bxacosta.restservice.repository.PersonaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaService {
    private final PersonaRepository personaRepository;

    public PersonaService(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    public Persona save(Persona persona) {
        return personaRepository.save(persona);
    }

    public Persona findOne(Integer id) {
        return personaRepository.findById(id).orElse(null);
    }

    public List<Persona> findAll() {
        return personaRepository.findAll();
    }

    public void delete(Integer id) {
        personaRepository.deleteById(id);
    }
}
