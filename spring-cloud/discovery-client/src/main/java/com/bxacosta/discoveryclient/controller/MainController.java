package com.bxacosta.discoveryclient.controller;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CommonsLog
@RestController
public class MainController {

    private final RestTemplate template;
    private final DiscoveryClient discoveryClient;

    public MainController(DiscoveryClient discoveryClient) {
        this.template = new RestTemplate();
        this.discoveryClient = discoveryClient;
    }

    @GetMapping("/service")
    public List<String> getServices() {
        return discoveryClient.getServices();
    }

    @GetMapping("/instance/{serviceId}")
    public List<ServiceInstance> getInstances(@PathVariable String serviceId) {
        return discoveryClient.getInstances(serviceId);
    }

    @GetMapping(value = "/test")
    public ResponseEntity<?> test() {
        var instances = discoveryClient.getInstances("service");

        StringBuilder sb = new StringBuilder();
        for (var instance : instances) {
            sb.append("\n");
            sb.append("-----------------------------------------------");
            sb.append("\n");
            sb.append("Host: ");
            sb.append(instance.getHost());
            sb.append("\n");
            sb.append("Port: ");
            sb.append(instance.getPort());
            sb.append("\n");
            sb.append("Uri: ");
            sb.append(instance.getUri());
            sb.append("\n");
            sb.append("-----------------------------------------------");
        }
        log.info(sb.toString());

        int index =  (int)(Math.random() * 10) % instances.size();

        var url = instances.get(index).getUri() + "/test";
        var resp = template.getForObject(url, String.class);

        Map<String, String> response = new HashMap<>();
        response.put("response", resp);
        response.put("uri", url);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
