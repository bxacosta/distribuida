package com.bxacosta;

import com.bxacosta.servicios.HolaMundo;

import javax.enterprise.inject.Instance;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;

public class PrincipalCdi {
    public static void main(String[] args) {
        SeContainerInitializer init = SeContainerInitializer.newInstance();
        SeContainer container = init.initialize();

        Instance<HolaMundo> obj = container.select(HolaMundo.class);
        HolaMundo servicio = obj.get();

        System.out.println(servicio.hola("Mundo"));
    }
}
