package com.bxacosta.servicios;

import javax.inject.Named;

@Named
public class HolaMundoImpl implements HolaMundo {

    @Override
    public String hola(String nombre) {
        return String.format("Hola %s", nombre);
    }
}