package com.bxacosta.web.service;

import com.bxacosta.web.model.Persona;
import com.bxacosta.web.model.TipoDireccion;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class PersonaService {

    @Value("${app.service.url}")
    private String SERVICE_URL;

    private final RestTemplate template;
    private final ObjectMapper mapper;

    public PersonaService(RestTemplate template, ObjectMapper mapper) {
        this.template = template;
        this.mapper = mapper;
    }

    public Persona save(Persona persona) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        var json = mapper.writeValueAsString(persona);
        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        ResponseEntity<Persona> response = this.template.exchange(
                SERVICE_URL + "/persona",
                HttpMethod.POST,
                entity,
                Persona.class);

        return response.getBody();
    }

    public void edit(Integer id, Persona persona) throws JsonProcessingException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        var json = mapper.writeValueAsString(persona);
        HttpEntity<String> entity = new HttpEntity<>(json, headers);

        this.template.exchange(
                SERVICE_URL + "/persona/" + persona.getId(),
                HttpMethod.PUT,
                entity,
                Void.class);
    }

    public Persona findOne(Integer id) {
        ResponseEntity<Persona> response = this.template.exchange(
                SERVICE_URL + "/persona/" + id,
                HttpMethod.GET,
                null,
                Persona.class);
        return response.getBody();
    }

    public List<Persona> findAllPersona() {
        ResponseEntity<List<Persona>> response = this.template.exchange(
                SERVICE_URL + "/persona",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Persona>>() {
                });

        return response.getBody();
    }

    public List<TipoDireccion> findAllTipo() {
        ResponseEntity<List<TipoDireccion>> response = this.template.exchange(
                SERVICE_URL + "/tipo",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<TipoDireccion>>() {
                });

        return response.getBody();
    }

    public void delete(Integer id) {
        this.template.delete(SERVICE_URL + "/persona/" + id);
    }
}
