package com.bxacosta.web.controller;

import com.bxacosta.web.model.Persona;
import com.bxacosta.web.service.PersonaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class IndexController {

    private final PersonaService personaService;

    public IndexController(PersonaService personaService) {
        this.personaService = personaService;
    }

    @GetMapping("/")
    public String index(@RequestParam(required = false) Integer id, Model model) {
        var persona = new Persona();
        if (!StringUtils.isEmpty(id)) {
            persona = personaService.findOne(id);
        }

        model.addAttribute("persona", persona);
        model.addAttribute("personas", personaService.findAllPersona());
        model.addAttribute("tipos", personaService.findAllTipo());

        return "index";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute Persona persona, Model model) throws JsonProcessingException {
        if (persona.getId() != null) {
            personaService.edit(persona.getId(), persona);
            index(persona.getId(), model);
            return "redirect:/";
        }

        persona = personaService.save(persona);
        index(persona != null ? persona.getId() : null, model);
        return "redirect:/";
    }

    @GetMapping("/delete")
    public String delete(@RequestParam Integer id, Model model) {
        personaService.delete(id);
        this.index(null, model);
        return "redirect:/";
    }
}
