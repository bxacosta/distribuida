package com.bxacosta.service;

import com.bxacosta.dto.Persona;
import com.bxacosta.mapper.PersonaMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaServiceImpl implements PersonaService {

    private final JdbcTemplate jdbcTemplate;

    public PersonaServiceImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Persona save(Persona item) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        if (item.getId() != null) {
            Persona persona = findById(item.getId());
            if (persona != null) {
                String query = "UPDATE persona SET nombre = ?, direccion = ?  WHERE id = ?";
                jdbcTemplate.update(query, item.getNombre(), item.getDireccion(), item.getId());
                return item;
            }
        }

        String query = "INSERT INTO persona (nombre, direccion) VALUES (?, ?)";
        jdbcTemplate.update(query, item.getNombre(), item.getDireccion());
        return item;
    }

    @Override
    public Persona findById(Integer id) {
        String query = "SELECT * FROM persona WHERE id = ?";
        return jdbcTemplate.queryForObject(query, new Object[]{id}, new PersonaMapper());
    }

    @Override
    public List<Persona> findAll() {
        String query = "SELECT * FROM persona";
        return jdbcTemplate.query(query, new PersonaMapper());
    }

    @Override
    public void delete(Integer id) {
        String query = "DELETE FROM persona WHERE id = ?";
        jdbcTemplate.update(query, id);
    }
}
