package com.bxacosta.dto;

import lombok.Data;

@Data
public class Persona {
    private Integer id;
    private String nombre;
    private String direccion;
}
