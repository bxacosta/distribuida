package com.bxacosta.config;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@CommonsLog
@Configuration
@ComponentScan(basePackages = "com.bxacosta.service")
public class DataServiceConfig {

    @Bean
    public DataSource dataSource() {
        try {
            EmbeddedDatabaseBuilder dbBuilder = new EmbeddedDatabaseBuilder();
            return dbBuilder.setType(EmbeddedDatabaseType.HSQL)
                    .generateUniqueName(true)
                    .addScripts("classpath:db/schema.sql", "classpath:db/data.sql")
                    .build();
        } catch (Exception e) {
            log.error("Embedded DataSource bean cannot be created!", e);
            return null;
        }
    }

//    @Bean
//    public DataSource dataSource() {
//        try {
//            DriverManagerDataSource dataSource = new DriverManagerDataSource();
//            dataSource.setDriverClassName("org.postgresql.Driver");
//            dataSource.setUrl("jdbc:postgresql://localhost:5432/distribuida");
//            dataSource.setUsername("postgres");
//            dataSource.setPassword("admin");
//
//            return dataSource;
//        } catch (Exception e) {
//            log.error("Postgresql DataSource bean cannot be created!", e);
//            return null;
//        }
//    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate();
        jdbcTemplate.setDataSource(dataSource());
        return jdbcTemplate;
    }
}
