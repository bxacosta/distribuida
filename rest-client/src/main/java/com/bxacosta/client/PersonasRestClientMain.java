package com.bxacosta.client;

import com.bxacosta.client.dto.Persona;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

public class PersonasRestClientMain {

    public static final String URL = "http://127.0.0.1:8080/app";

    private static void testJson() throws Exception {

        System.out.println("------------------------------------------------");
        System.out.println("-- Test json");

        Client client = ClientBuilder.newClient();

        //----------------------------------------------------
        // obtener el typo de dato como String: json
        String responseJson = client.target(URL)
                .path("/personas/{id}")
                .resolveTemplate("id", 10)
                .request(MediaType.APPLICATION_JSON)
                .get(String.class);

        System.out.println("json string: " + responseJson);

        // deserializar el JSON
        ObjectMapper mapper = new ObjectMapper();
        Persona p = mapper.readValue(responseJson, Persona.class);

        System.out.println("id    : " + p.getId());
        System.out.println("nombre: " + p.getNombre());
    }

    private static void testXml() throws Exception {

        System.out.println("------------------------------------------------");
        System.out.println("-- Test xml");

        Client client = ClientBuilder.newClient();

        // obtener el typo de dato como String: xml
        String responseXml = client.target(URL)
                .path("/personas/{id}")
                .resolveTemplate("id", 10)
                .request(MediaType.APPLICATION_XML)
                .get(String.class);

        System.out.println("xml string: " + responseXml);

        // deserializar el XML
        JAXBContext jaxbContext = JAXBContext.newInstance(Persona.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

        Persona p = (Persona) jaxbUnmarshaller.unmarshal(new StringReader(responseXml));

        System.out.println("id    : " + p.getId());
        System.out.println("nombre: " + p.getNombre());
    }

    private static void testJson2Persona() throws Exception {

        System.out.println("------------------------------------------------");
        System.out.println("-- Test json==>persona");

        Client client = ClientBuilder.newClient()
                .register(new JacksonJsonProvider());

        //----------------------------------------------------
        // obtener el typo de dato como String: xml
        Persona p = client.target(URL)
                .path("/personas/{id}")
                .resolveTemplate("id", 10)
                .request(MediaType.APPLICATION_JSON)
                .get(Persona.class);

        System.out.println("id    : " + p.getId());
        System.out.println("nombre: " + p.getNombre());
    }

    private static void testXml2Persona() throws Exception {

        System.out.println("------------------------------------------------");
        System.out.println("-- Test xml==>persona");

        Client client = ClientBuilder.newClient();

        //----------------------------------------------------
        // obtener el typo de dato como String: xml
        Persona p = client.target(URL)
                .path("/personas/{id}")
                .resolveTemplate("id", 10)
                .request(MediaType.APPLICATION_XML)
                .get(Persona.class);

        System.out.println("id    : " + p.getId());
        System.out.println("nombre: " + p.getNombre());
    }

    public static void main(String[] args) throws Exception {

//        testJson();
//
        testXml();
//
//        testJson2Persona();
//
//        testXml2Persona();

    }

}
