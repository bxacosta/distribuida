package com.bxacosta.service;

import com.bxacosta.dto.Persona;

import javax.ejb.Remote;
import java.util.List;

@Remote
public interface PersonaService {
    Persona save(Persona item);
    Persona findById(Integer id);
    List<Persona> findAll();
    void delete(Integer id);
}
