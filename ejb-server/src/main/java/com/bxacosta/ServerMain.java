package com.bxacosta;

import com.bxacosta.config.ServerConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class ServerMain {

    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(ServerConfig.class);
        context.refresh();

        System.out.println("Servidor Iniciado...");
        System.in.read();

        context.close();
    }
}
