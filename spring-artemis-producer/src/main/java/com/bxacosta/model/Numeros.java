package com.bxacosta.model;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class Numeros implements Serializable {

    private Integer n1;
    private Integer n2;

    public Numeros() {
        this.n1 = 0;
        this.n2 = 0;
    }

    public Numeros(Integer n1, Integer n2) {
        this.n1 = n1;
        this.n2 = n2;
    }

    public Integer getN1() {
        return n1;
    }

    public void setN1(Integer n1) {
        this.n1 = n1;
    }

    public Integer getN2() {
        return n2;
    }

    public void setN2(Integer n2) {
        this.n2 = n2;
    }

    @Override
    public String toString() {
        return "Numeros{" +
                "n1=" + n1 +
                ", n2=" + n2 +
                '}';
    }
}
