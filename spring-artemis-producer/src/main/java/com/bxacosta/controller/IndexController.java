package com.bxacosta.controller;


import com.bxacosta.model.Numeros;
import com.bxacosta.service.MessageSender;
import com.bxacosta.service.ResultService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class IndexController implements Serializable {

    private MessageSender messageSender;
    private ResultService resultService;

    private String result;

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct IndexController");

        ApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
        messageSender = ctx.getBean(MessageSender.class);
        resultService = ctx.getBean(ResultService.class);

    }

    public void enviar(Numeros numeros) {
        messageSender.sendMessage(numeros);
    }

    public void resultado() {
        result = resultService.getResult();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
