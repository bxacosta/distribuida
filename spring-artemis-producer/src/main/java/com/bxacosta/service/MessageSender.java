package com.bxacosta.service;

import com.bxacosta.model.Numeros;

public interface MessageSender {

    void sendMessage(String message);

    void sendMessage(Numeros numeros);

}
