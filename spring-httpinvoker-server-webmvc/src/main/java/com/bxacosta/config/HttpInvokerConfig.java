package com.bxacosta.config;

import com.bxacosta.service.ServicioOperaciones;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
public class HttpInvokerConfig {

    private final ServicioOperaciones operaciones;

    public HttpInvokerConfig(ServicioOperaciones operaciones) {
        this.operaciones = operaciones;
    }

    @Bean(name = "/operaciones")
    public HttpInvokerServiceExporter httpInvokerServiceExporter() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();

        exporter.setService(operaciones);
        exporter.setServiceInterface(ServicioOperaciones.class);

        return exporter;
    }
}
