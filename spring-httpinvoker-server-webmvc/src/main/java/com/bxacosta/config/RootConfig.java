package com.bxacosta.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackages = "com.bxacosta.service")
public class RootConfig {
}
