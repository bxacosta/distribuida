package com.bxacosta.restserver;

import com.bxacosta.restserver.servicio.HolaMundoImpl;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/app")
public class RestServerApplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {

        Set<Class<?>> classes = new HashSet<Class<?>>();

        classes.add(HolaMundoImpl.class);

        //providers
        classes.add(JacksonJsonProvider.class);

        return classes;
    }

}
